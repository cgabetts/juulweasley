package hogwarts.locations;

import hogwarts.locations.corridors.GreatHallCorridor;
import hogwarts.locations.corridors.LibraryCorridor;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class LocationTest {

    @Test
    public void shouldConvertNameOfClassIntoOptionText() {
        GreatHallCorridor greatHallCorridor = new GreatHallCorridor();

        String optionText = greatHallCorridor.getOptionText(new LibraryCorridor());

        assertThat(optionText, is("Go to the Great hall corridor"));
    }
}