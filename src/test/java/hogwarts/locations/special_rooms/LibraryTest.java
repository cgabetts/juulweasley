package hogwarts.locations.special_rooms;

import hogwarts.characters.DrWhoInLibrary;
import hogwarts.investigatables.Investigatible;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

public class LibraryTest {

    @Test
    public void shouldRemoveInvestigatableAfterItsBeenInvestigated() {
        Library library = new Library();

        List<Investigatible> investigatables = library.getInvestigatables();
        Investigatible firstInvestigatable = investigatables.get(0);

        library.investigated(firstInvestigatable);

        assertThat(library.getInvestigatables().isEmpty(), is(true));
    }

    @Test
    public void shouldRemoveDrWhoOnExit() {
        Library library = new Library();
        DrWhoInLibrary drWhoInLibrary = new DrWhoInLibrary();
        drWhoInLibrary.onStartOfConversation(library);

        library.addCharacter(drWhoInLibrary);

        library.onExit();

        assertTrue(library.getCharactersPresent().isEmpty());
    }
}