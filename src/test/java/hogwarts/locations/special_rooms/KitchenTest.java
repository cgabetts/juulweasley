package hogwarts.locations.special_rooms;

import acceptance.TestReader;
import hogwarts.locations.GreatHall;
import hogwarts.locations.common_rooms.HufflepufCommonRoom;
import hogwarts.locations.corridors.GreatHallCorridor;
import hogwarts.terminal.Terminal;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class KitchenTest {
    private TestReader testReader;

    @Before
    public void setUp() throws Exception {
        testReader = new TestReader();
        Terminal.setReader(testReader);
    }

    @Test
    public void shouldShowOptionTextAsPaintingWhenInGreatHallCorridor() {
        Kitchen kitchen = new Kitchen();

        String optionText = kitchen.getOptionText(new GreatHallCorridor());

        assertThat(optionText, is("Investigate the painting of a fruit bowl"));
    }

    @Test
    public void shouldShowOptionTextAsNavigateToKitchenWhenInHufflepufCommonRoom() {
        Kitchen kitchen = new Kitchen();

        String optionText = kitchen.getOptionText(new HufflepufCommonRoom());

        assertThat(optionText, is("Go to the Kitchen"));
    }

    @Test
    public void shouldAutomaticallyGrantAccessWhenApproachingKitchenFromCommonRoom() {
        Kitchen kitchen = new Kitchen();

        boolean accessGranted = kitchen.passAccessChallenge(new HufflepufCommonRoom());

        assertThat(accessGranted, is(true));
    }

    @Test
    public void shouldGrantAccessWhenComingFromGreatHallWhenEnteringTicklePear() {
        testReader.setInput("Tickle the pear");

        Kitchen kitchen = new Kitchen();

        boolean accessGranted = kitchen.passAccessChallenge(new GreatHallCorridor());

        assertThat(accessGranted, is(true));
    }

    @Test
    public void shouldNotGrantAccessWhenComingFromGreatHallWhenNotEnteringTicklePear() {
        testReader.setInput("Tickle the apple");

        Kitchen kitchen = new Kitchen();

        boolean accessGranted = kitchen.passAccessChallenge(new GreatHallCorridor());

        assertThat(accessGranted, is(false));
    }
}