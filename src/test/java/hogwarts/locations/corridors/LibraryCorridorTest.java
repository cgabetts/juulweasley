package hogwarts.locations.corridors;

import hogwarts.investigatables.InvestigateBehindPainting;
import hogwarts.investigatables.InvestigatePaintingInLibraryCorridor;
import hogwarts.investigatables.Investigatible;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LibraryCorridorTest {

    @Mock
    private InvestigateBehindPainting investigateBehindPainting;

    @Test
    public void afterPaintingHasBeenSpokenToOptionToSearchBehindPaintingShouldBeAdded() {
        LibraryCorridor libraryCorridor = new LibraryCorridor();

        List<Investigatible> investigatables = libraryCorridor.getInvestigatables();
        assertThat(investigatables, hasItem(instanceOf(InvestigatePaintingInLibraryCorridor.class)));

        libraryCorridor.investigated(investigatables.get(0));

        assertThat(libraryCorridor.getInvestigatables(), hasItem(instanceOf(InvestigateBehindPainting.class)));
    }

    @Test
    public void afterFinishedWithBehindPaintingFactItShouldDisappear() {
        when(investigateBehindPainting.isFinished()).thenReturn(true);
        LibraryCorridor libraryCorridor = new LibraryCorridor();

        List<Investigatible> investigatables = libraryCorridor.getInvestigatables();
        investigatables.add(investigateBehindPainting);

        libraryCorridor.investigated(investigateBehindPainting);

        assertThat(libraryCorridor.getInvestigatables(), not(hasItem(investigateBehindPainting)));
    }

    @Test
    public void afterNotBeingFinishedWithBehindPaintingFactItShouldRemain() {
        when(investigateBehindPainting.isFinished()).thenReturn(false);

        LibraryCorridor libraryCorridor = new LibraryCorridor();

        List<Investigatible> investigatables = libraryCorridor.getInvestigatables();
        investigatables.add(investigateBehindPainting);

        libraryCorridor.investigated(investigateBehindPainting);

        assertThat(libraryCorridor.getInvestigatables(), hasItem(investigateBehindPainting));
    }
}