package hogwarts.investigatables;

import acceptance.TestReader;
import hogwarts.ending.End;
import hogwarts.terminal.Terminal;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class InvestigateBehindPaintingTest {
    private TestReader testReader;

    @Before
    public void setUp() throws Exception {
        testReader = new TestReader();
        Terminal.setReader(testReader);
    }


    @Test
    public void shouldMarkFactAsFinishedIfOption3WasChosen() {
        testReader.setInput("3");

        InvestigateBehindPainting investigateBehindPainting = new InvestigateBehindPainting();

        investigateBehindPainting.investigate();

        assertTrue(investigateBehindPainting.isFinished());
    }

    @Test
    public void shouldTriggerEndingWhenOption2IsChosen() {
        testReader.setInput("2");
        InvestigateBehindPainting investigateBehindPainting = new InvestigateBehindPainting();

        investigateBehindPainting.investigate();

        assertTrue(End.hasEnded());
    }

    @Test
    public void shouldNotMarkFactAsFinishedIfOption1WasChosen() {
        testReader.setInput("1");

        InvestigateBehindPainting investigateBehindPainting = new InvestigateBehindPainting();

        investigateBehindPainting.investigate();

        assertFalse(investigateBehindPainting.isFinished());
    }
}