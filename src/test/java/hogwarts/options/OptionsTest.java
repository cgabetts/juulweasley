package hogwarts.options;

import acceptance.TestReader;
import hogwarts.locations.GreatHall;
import hogwarts.locations.corridors.GreatHallCorridor;
import hogwarts.terminal.Terminal;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class OptionsTest {

    private TestReader testReader;

    @Before
    public void setUp() throws Exception {
        testReader = new TestReader();
        Terminal.setReader(testReader);
    }

    @Test
    public void shouldBeAbleToReturnAnOptionBasedOnLocation() {
        testReader.setInput("1");

        GreatHall greatHall = new GreatHall();
        Options options = new Options(greatHall);

        Optionable optionable = options.selectOption();

        assertThat(optionable, instanceOf(GreatHallCorridor.class));
    }

    @Test
    public void shouldReturnNullIfNotAViableAction() {
        testReader.setInput("§");

        GreatHall greatHall = new GreatHall();
        Options options = new Options(greatHall);

        Optionable optionable = options.selectOption();

        assertThat(optionable, nullValue());
    }

    @Test
    public void shouldReturnNullIfNumberNotValid() {
        testReader.setInput("2");

        GreatHall greatHall = new GreatHall();
        Options options = new Options(greatHall);

        Optionable optionable = options.selectOption();

        assertThat(optionable, nullValue());
    }

}