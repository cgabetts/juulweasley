package acceptance;

import hogwarts.Juul;
import hogwarts.locations.Hogwarts;
import hogwarts.terminal.Terminal;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NavigationTest {

    private TestPrinter printer;
    private TestReader testReader;

    @Before
    public void setUp() throws Exception {
        printer = new TestPrinter();
        Terminal.setPrinter(printer);

        testReader = new TestReader();
        Terminal.setReader(testReader);
    }

    @Test
    public void shouldLetJuulNavigateToADifferentArea() {
        Juul juul = new Juul();
        juul.navigateToLocation(Hogwarts.GREAT_HALL_CORRIDOR);

        testReader.setInput("1");
        juul.performActions();

        assertThat(juul.getCurrentLocation(), is(Hogwarts.LIBRARY_CORRIDOR));
    }
}
