package acceptance;

import hogwarts.terminal.Printer;

import java.util.ArrayList;
import java.util.List;

public class TestPrinter extends Printer {

    private List<String> printedText = new ArrayList<>();

    public void print(String text) {
        printedText.add(text);
    }


    public List<String> getPrintedText() {
        return printedText;
    }
}
