package acceptance;

import hogwarts.terminal.Reader;

public class TestReader extends Reader {

    private String input;

    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public String readLine() {
        return input;
    }
}
