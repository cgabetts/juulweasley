package hogwarts.options;

import hogwarts.characters.InteractableCharacter;
import hogwarts.investigatables.Investigatible;
import hogwarts.locations.Location;
import hogwarts.terminal.Terminal;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class Options {
    private Location currentLocation;

    public Options(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Optionable selectOption() {
        List<Investigatible> investigatables = currentLocation.getInvestigatables();
        List<Location> adjacentLocations = currentLocation.getAdjacentLocations();
        List<InteractableCharacter> charactersPresent = currentLocation.getCharactersPresent();

        List<Optionable> optionableList = Stream.of(investigatables, adjacentLocations, charactersPresent)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        for (int i = 0; i < optionableList.size(); i++) {
            String displayedOptionNumber = String.valueOf(i + 1);
            Terminal.printToScreen(format("Option %s: %s", displayedOptionNumber, optionableList.get(i).getOptionText(currentLocation)));
        }

        String inputtedChar = Terminal.readCharacter();
        if(!inputtedChar.matches("[0-9]")) {
            return null;
        }

        int chosenOption = Integer.valueOf(inputtedChar) - 1;

        if (chosenOption < 0 || chosenOption > optionableList.size() - 1) return null;
        return optionableList.get(chosenOption);
    }
}
