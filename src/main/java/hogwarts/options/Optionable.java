package hogwarts.options;

import hogwarts.locations.Location;

public interface Optionable {
    String getOptionText(Location currentLocation);
}
