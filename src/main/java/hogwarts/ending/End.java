package hogwarts.ending;

import hogwarts.terminal.Terminal;

public class End {

    private static boolean hasEnded = false;
    private static Ending ending;

    public static void triggerEnding(Ending triggeredEnding) {
        hasEnded = true;
        ending = triggeredEnding;
    }

    public static boolean hasEnded() {
        return hasEnded;
    }

    public static void printEnding() {
        Terminal.printFromFile(ending.getFileLocation());
    }
}
