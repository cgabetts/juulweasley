package hogwarts.ending;

public enum Ending {

    KISSED_BY_DEMENTOR("endings/kissed-by-dementor.txt"),
    HIT_BY_BRICK("endings/hit-by-brick.txt");

    private final String fileLocation;

    Ending(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileLocation() {
        return fileLocation;
    }
}
