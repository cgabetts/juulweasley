package hogwarts.story;

import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;

public class LocationOfTheDoctor {

    private static Location currentLocation = Hogwarts.LIBRARY;

    public static boolean isDoctorPresentAtLocation(Location currentLocation) {
        return currentLocation.equals(LocationOfTheDoctor.currentLocation);
    }

    public static void setLocationOfDoctorTo(Location newLocation) {
        currentLocation = newLocation;
    }

}
