package hogwarts.investigatables;

import hogwarts.ending.End;
import hogwarts.ending.Ending;
import hogwarts.locations.Location;
import hogwarts.terminal.Terminal;

public class InvestigateBehindPainting implements Investigatible {

    private boolean finished = false;

    @Override
    public String getOptionText(Location currentLocation) {
        return "Investigate behind the painting";
    }

    @Override
    public void investigate() {
        Terminal.printFromFile("library-corridor-behind-painting.txt");
        String input = Terminal.readCharacter();

        switch (input) {
            case "1":
                Terminal.printToScreen("A spell of opening! " +
                        "The spell bounces off the brick, showering you in brickdust. Your face is now dirty. " +
                        "There is nothing here to open you silly wizard!");
                break;
            case "2":
                End.triggerEnding(Ending.HIT_BY_BRICK);
                break;
            case "3":
                Terminal.printFromFile("library-corridor-find-stone.txt");
                finished = true;
        }
    }

    public boolean isFinished() {
        return finished;
    }
}
