package hogwarts.investigatables;

import hogwarts.locations.Location;
import hogwarts.terminal.Terminal;

public class InvestigatePaintingInLibraryCorridor implements Investigatible {
    @Override
    public String getOptionText(Location currentLocation) {
        return "Ask the painting what happened";
    }

    @Override
    public void investigate() {
        Terminal.printFromFile("library-corridor-painting-dialogue.txt");
    }
}
