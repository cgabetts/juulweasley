package hogwarts.investigatables;

import hogwarts.characters.DrWhoInLibrary;
import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;
import hogwarts.locations.special_rooms.Library;
import hogwarts.story.LocationOfTheDoctor;
import hogwarts.terminal.Terminal;

public class StrangeNoiseInLibrary implements Investigatible {

    @Override
    public String getOptionText(Location currentLocation) {
        return "Investigate the strange noise";
    }

    @Override
    public void investigate() {
        Terminal.printFromFile("strange-noise-investigation.txt");
        Library library = Hogwarts.LIBRARY;
        library.addCharacter(new DrWhoInLibrary());
        LocationOfTheDoctor.setLocationOfDoctorTo(library);
    }
}
