package hogwarts.investigatables;

import hogwarts.options.Optionable;

public interface Investigatible extends Optionable {
    void investigate();
}
