package hogwarts.terminal;

import java.util.Scanner;

public class Reader {

    public String readLine() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
