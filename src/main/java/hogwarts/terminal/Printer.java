package hogwarts.terminal;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

public class Printer {

    public void print(String text) {
        System.out.println(text);
    }

    public void printFile(String location) {
        try (Stream<String> lines = Files.lines(Paths.get(Objects.requireNonNull(ClassLoader.getSystemClassLoader().getResource(location)).getPath()))) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
