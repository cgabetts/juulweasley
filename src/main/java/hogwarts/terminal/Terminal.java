package hogwarts.terminal;

public class Terminal {

    private static Printer printer = new Printer();
    private static Reader reader = new Reader();

    public static void printToScreen(String text) {
        printer.print(text);
    }

    public static void printFromFile(String location) {
        printer.printFile(location);
    }

    public static String readCharacter() {
        return reader.readLine();
    }

    public static void setPrinter(Printer printer) {
        Terminal.printer = printer;
    }

    public static void setReader(Reader reader) {
        Terminal.reader = reader;
    }
}
