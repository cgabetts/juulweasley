package hogwarts;

import hogwarts.ending.End;
import hogwarts.terminal.Terminal;

public class Main {

    public static void main(String[] args) {
        Terminal.printFromFile("introduction.txt");

        Juul juul = new Juul();

        while (!End.hasEnded()) {
            juul.performActions();
        }

        End.printEnding();
        Terminal.printToScreen("Bye bye!");
    }
}

