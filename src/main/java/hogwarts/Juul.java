package hogwarts;

import hogwarts.characters.InteractableCharacter;
import hogwarts.investigatables.Investigatible;
import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;
import hogwarts.locations.special_rooms.Library;
import hogwarts.options.Optionable;
import hogwarts.options.Options;
import hogwarts.terminal.Terminal;

public class Juul {

    private Location currentLocation = Hogwarts.LIBRARY;

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void navigateToLocation(Location location) {
        if(location.passAccessChallenge(currentLocation)) {
            currentLocation = location;
            location.onEntry();
        }
    }

    public void performActions() {
        Terminal.printToScreen("What will you do next?");

        Options options = new Options(currentLocation);
        Optionable selectedOption = options.selectOption();

        if (selectedOption instanceof Location) {
            navigateToLocation((Location) selectedOption);
        } else if (selectedOption instanceof InteractableCharacter) {
            InteractableCharacter character = (InteractableCharacter) selectedOption;
            character.onStartOfConversation(currentLocation);
        } else if (selectedOption instanceof Investigatible) {
            Investigatible investigatible = (Investigatible) selectedOption;
            investigatible.investigate();
            currentLocation.investigated(investigatible);
        }
        else {
            Terminal.printToScreen("I don't know what you mean! Please pick from the options.");
        }
    }
}
