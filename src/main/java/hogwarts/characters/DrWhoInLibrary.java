package hogwarts.characters;
import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;
import hogwarts.locations.special_rooms.Library;
import hogwarts.terminal.Terminal;

public class DrWhoInLibrary implements InteractableCharacter {
    private boolean firstInteraction = true;

    @Override
    public String getOptionText(Location currentLocation) {
        return "Talk to the strange man";
    }

    @Override
    public void onStartOfConversation(Location currentLocation) {
        if (firstInteraction) {
            Terminal.printFromFile("dr-who-conversation-in-library.txt");
            Library library = (Library) currentLocation;
            library.addAdjacentLocation(Hogwarts.LIBRARY_CORRIDOR);
        } else {
            Terminal.printToScreen("The strange man is looking around impatiently");
        }

        firstInteraction = false;
    }

    public boolean hasInteracted() {
        return !firstInteraction;
    }
}
