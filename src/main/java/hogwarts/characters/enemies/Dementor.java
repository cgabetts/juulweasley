package hogwarts.characters.enemies;

import hogwarts.ending.Ending;

import static hogwarts.ending.End.triggerEnding;

public class Dementor {
    public void attack() {
        triggerEnding(Ending.KISSED_BY_DEMENTOR);
    }
}
