package hogwarts.characters;

import hogwarts.locations.Location;
import hogwarts.options.Optionable;

public interface InteractableCharacter extends Optionable {

    void onStartOfConversation(Location currentLocation);

}
