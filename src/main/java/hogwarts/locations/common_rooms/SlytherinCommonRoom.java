package hogwarts.locations.common_rooms;

import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;

import java.util.List;

import static java.util.Arrays.asList;

public class SlytherinCommonRoom implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(Hogwarts.WEST_WING);
    }
}
