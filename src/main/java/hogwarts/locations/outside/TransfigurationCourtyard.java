package hogwarts.locations.outside;

import hogwarts.locations.Location;
import hogwarts.terminal.Terminal;

import java.util.List;

import static hogwarts.locations.Hogwarts.*;
import static java.util.Arrays.asList;

public class TransfigurationCourtyard implements Location {
    @Override
    public List<Location> getAdjacentLocations() {
        return asList(LIBRARY_CORRIDOR, MCGONAGALLS_OFFICE, EAST_WING_CORRIDOR);
    }

    @Override
    public void onEntry() {
        Terminal.printFromFile("transfiguration-courdyard-on-entry.txt");
    }
}
