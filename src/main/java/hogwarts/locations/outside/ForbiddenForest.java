package hogwarts.locations.outside;

import hogwarts.characters.enemies.Dementor;
import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;

import java.util.List;

import static java.util.Arrays.asList;

public class ForbiddenForest implements Location {

    @Override
    public void onEntry() {
        new Dementor().attack();
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(Hogwarts.EAST_WING);
    }
}
