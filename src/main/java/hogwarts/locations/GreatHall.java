package hogwarts.locations;

import hogwarts.characters.InteractableCharacter;
import hogwarts.terminal.Terminal;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

public class GreatHall implements Location {

    @Override
    public void onEntry() {
        Terminal.printToScreen("Juul entered the Great Hall");
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(Hogwarts.GREAT_HALL_CORRIDOR);
    }

    @Override
    public List<InteractableCharacter> getCharactersPresent() {
        return Collections.emptyList();
    }
}
