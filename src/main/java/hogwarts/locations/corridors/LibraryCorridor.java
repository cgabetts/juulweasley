package hogwarts.locations.corridors;

import hogwarts.investigatables.InvestigateBehindPainting;
import hogwarts.investigatables.InvestigatePaintingInLibraryCorridor;
import hogwarts.investigatables.Investigatible;
import hogwarts.locations.Location;
import hogwarts.story.LocationOfTheDoctor;
import hogwarts.terminal.Terminal;

import java.util.ArrayList;
import java.util.List;

import static hogwarts.locations.Hogwarts.GREAT_HALL_CORRIDOR;
import static hogwarts.locations.Hogwarts.LIBRARY;
import static hogwarts.locations.Hogwarts.TRANSFIGURATION_COURTYARD;
import static java.util.Arrays.asList;

public class LibraryCorridor implements Location {
    private boolean firstVisit = true;
    private List<Investigatible> investigatibles = new ArrayList<>();

    public LibraryCorridor() {
        investigatibles.add(new InvestigatePaintingInLibraryCorridor());
    }

    @Override
    public void onEntry() {
        Terminal.printFromFile("library-corridor-on-entry.txt");
        if (firstVisit) {
            Terminal.printFromFile("library-corridor-with-doctor.txt");
            firstVisit = false;
        }
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(TRANSFIGURATION_COURTYARD, GREAT_HALL_CORRIDOR, LIBRARY);
    }

    @Override
    public List<Investigatible> getInvestigatables() {
        return investigatibles;
    }

    @Override
    public String getOptionText(Location currentLocation) {
        if (LocationOfTheDoctor.isDoctorPresentAtLocation(LIBRARY)) {
            return "Follow the Dr";
        }

        return Location.super.getOptionText(currentLocation);
    }

    @Override
    public void investigated(Investigatible investigatible) {
        if (investigatible instanceof InvestigatePaintingInLibraryCorridor) {
            investigatibles.remove(investigatible);
            investigatibles.add(new InvestigateBehindPainting());
        }

        if (investigatible instanceof InvestigateBehindPainting) {
            if (((InvestigateBehindPainting) investigatible).isFinished()) {
                investigatibles.remove(investigatible);
            }
        }
    }
}
