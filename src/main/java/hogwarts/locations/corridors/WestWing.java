package hogwarts.locations.corridors;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.SLYTHERIN_COMMON_ROOM;
import static hogwarts.locations.Hogwarts.TAPESTRY_CORRIDOR;
import static java.util.Arrays.asList;

public class WestWing implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(TAPESTRY_CORRIDOR, SLYTHERIN_COMMON_ROOM);
    }
}
