package hogwarts.locations.corridors;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.GREAT_HALL_CORRIDOR;
import static hogwarts.locations.Hogwarts.GRYFFINDOR_COMMON_ROOM;
import static hogwarts.locations.Hogwarts.HEADMASTERS_OFFICE;
import static hogwarts.locations.Hogwarts.OBSERVATORY;
import static java.util.Arrays.asList;

public class Stairwell implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(GRYFFINDOR_COMMON_ROOM, OBSERVATORY, HEADMASTERS_OFFICE, GREAT_HALL_CORRIDOR);
    }
}
