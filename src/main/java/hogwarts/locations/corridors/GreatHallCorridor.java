package hogwarts.locations.corridors;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.GREAT_HALL;
import static hogwarts.locations.Hogwarts.KITCHEN;
import static hogwarts.locations.Hogwarts.LIBRARY_CORRIDOR;
import static hogwarts.locations.Hogwarts.STAIRWELL;
import static hogwarts.locations.Hogwarts.TAPESTRY_CORRIDOR;
import static java.util.Arrays.asList;

public class GreatHallCorridor implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(LIBRARY_CORRIDOR, STAIRWELL, GREAT_HALL, TAPESTRY_CORRIDOR, KITCHEN);
    }
}
