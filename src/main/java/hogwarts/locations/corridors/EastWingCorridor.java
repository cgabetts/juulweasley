package hogwarts.locations.corridors;

import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;
import hogwarts.terminal.Terminal;

import java.util.List;

import static java.util.Arrays.asList;

public class EastWingCorridor implements Location {

    @Override
    public void onEntry() {
        Terminal.printFromFile("east-wing-corridor-on-entry.txt");
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(Hogwarts.EAST_WING, Hogwarts.ROOM_OF_REQUIREMENTS, Hogwarts.TRANSFIGURATION_COURTYARD);
    }
}
