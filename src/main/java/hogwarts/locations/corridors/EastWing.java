package hogwarts.locations.corridors;

import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;

import java.util.List;

import static java.util.Arrays.asList;

public class EastWing implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(Hogwarts.GREENHOUSE, Hogwarts.FORBIDDEN_FOREST);
    }
}
