package hogwarts.locations.corridors;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.GREAT_HALL_CORRIDOR;
import static hogwarts.locations.Hogwarts.SNAPES_CUPBOARD;
import static hogwarts.locations.Hogwarts.WEST_WING;
import static java.util.Arrays.asList;

public class TapestryCorridor implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(GREAT_HALL_CORRIDOR, SNAPES_CUPBOARD, WEST_WING);
    }
}
