package hogwarts.locations;

import hogwarts.locations.common_rooms.GryffindorCommonRoom;
import hogwarts.locations.common_rooms.HufflepufCommonRoom;
import hogwarts.locations.common_rooms.RavenclawCommonRoom;
import hogwarts.locations.common_rooms.SlytherinCommonRoom;
import hogwarts.locations.corridors.EastWing;
import hogwarts.locations.corridors.EastWingCorridor;
import hogwarts.locations.corridors.GreatHallCorridor;
import hogwarts.locations.corridors.LibraryCorridor;
import hogwarts.locations.corridors.Stairwell;
import hogwarts.locations.corridors.TapestryCorridor;
import hogwarts.locations.corridors.WestWing;
import hogwarts.locations.outside.ForbiddenForest;
import hogwarts.locations.outside.TransfigurationCourtyard;
import hogwarts.locations.special_rooms.Greenhouse;
import hogwarts.locations.special_rooms.HeadmastersOffice;
import hogwarts.locations.special_rooms.Kitchen;
import hogwarts.locations.special_rooms.Library;
import hogwarts.locations.special_rooms.McGonagallsOffice;
import hogwarts.locations.special_rooms.Observatory;
import hogwarts.locations.special_rooms.RoomOfRequirements;
import hogwarts.locations.special_rooms.SnapesCupboard;

public class Hogwarts {
    public static GreatHall GREAT_HALL = new GreatHall();
    public static Library LIBRARY = new Library();

    public static Location FORBIDDEN_FOREST = new ForbiddenForest();
    public static Location TRANSFIGURATION_COURTYARD = new TransfigurationCourtyard();

    public static Location LIBRARY_CORRIDOR = new LibraryCorridor();
    public static Location EAST_WING = new EastWing();
    public static Location EAST_WING_CORRIDOR = new EastWingCorridor();
    public static Location GREAT_HALL_CORRIDOR = new GreatHallCorridor();
    public static Location STAIRWELL = new Stairwell();
    public static Location TAPESTRY_CORRIDOR = new TapestryCorridor();
    public static Location WEST_WING = new WestWing();

    public static Location GRYFFINDOR_COMMON_ROOM = new GryffindorCommonRoom();
    public static Location SLYTHERIN_COMMON_ROOM = new SlytherinCommonRoom();
    public static Location RAVENCLAW_COMMON_ROOM = new RavenclawCommonRoom();
    public static Location HUFFLEPUF_COMMON_ROOM = new HufflepufCommonRoom();

    public static Location GREENHOUSE = new Greenhouse();
    public static Location HEADMASTERS_OFFICE = new HeadmastersOffice();
    public static Location KITCHEN = new Kitchen();
    public static Location MCGONAGALLS_OFFICE = new McGonagallsOffice();
    public static Location OBSERVATORY = new Observatory();
    public static Location ROOM_OF_REQUIREMENTS = new RoomOfRequirements();
    public static Location SNAPES_CUPBOARD = new SnapesCupboard();

}
