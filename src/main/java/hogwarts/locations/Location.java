package hogwarts.locations;

import hogwarts.characters.InteractableCharacter;
import hogwarts.investigatables.Investigatible;
import hogwarts.options.Optionable;

import java.util.Collections;
import java.util.List;

import static java.lang.String.format;

public interface Location extends Optionable {
    default boolean passAccessChallenge(Location currentLocation) { return true; }
    default void onEntry() {}
    default void onExit() {}
    default List<Location> getAdjacentLocations(){
        return Collections.emptyList();
    }
    default List<InteractableCharacter> getCharactersPresent() {
        return Collections.emptyList();
    }
    default List<Investigatible> getInvestigatables() { return Collections.emptyList(); }

    default String getOptionText(Location currentLocation) {
        String simpleName = this.getClass().getSimpleName();
        String spacesAdded = simpleName.replaceAll("(\\p{Ll})(\\p{Lu})","$1 $2");
        return format("Go to the %s%s", spacesAdded.substring(0,1), spacesAdded.substring(1).toLowerCase());
    }

    default void investigated(Investigatible investigatible) {};
    default void addCharacter(InteractableCharacter character) {};

    default void addAdjacentLocation(Location location) {};
}
