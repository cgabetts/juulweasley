package hogwarts.locations.special_rooms;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.STAIRWELL;
import static java.util.Arrays.asList;

public class Observatory implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(STAIRWELL);
    }
}
