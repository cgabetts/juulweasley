package hogwarts.locations.special_rooms;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.EAST_WING_CORRIDOR;
import static java.util.Arrays.asList;

public class RoomOfRequirements implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(EAST_WING_CORRIDOR);
    }
}
