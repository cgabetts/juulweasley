package hogwarts.locations.special_rooms;

import hogwarts.characters.InteractableCharacter;
import hogwarts.investigatables.Investigatible;
import hogwarts.investigatables.StrangeNoiseInLibrary;
import hogwarts.locations.Hogwarts;
import hogwarts.locations.Location;
import hogwarts.story.LocationOfTheDoctor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static hogwarts.locations.Hogwarts.LIBRARY_CORRIDOR;
import static java.util.Arrays.asList;

public class Library implements Location {

    private List<Investigatible> investigatibles = new ArrayList<>();
    private List<InteractableCharacter> characters = new ArrayList<>();
    private List<Location> locations = new ArrayList<>();

    public Library() {
        investigatibles.add(new StrangeNoiseInLibrary());
    }

    @Override
    public void onExit() {
        characters = new ArrayList<>();
        LocationOfTheDoctor.setLocationOfDoctorTo(Hogwarts.LIBRARY_CORRIDOR);
    }

    @Override
    public List<Investigatible> getInvestigatables() {
        return investigatibles;
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return locations;
    }

    @Override
    public List<InteractableCharacter> getCharactersPresent() {
        return characters;
    }

    @Override
    public void investigated(Investigatible investigatible) {
        investigatibles.remove(investigatible);
    }

    @Override
    public void addCharacter(InteractableCharacter character) {
        characters.add(character);
    }

    @Override
    public void addAdjacentLocation(Location location) {
        locations.add(location);
    }
}
