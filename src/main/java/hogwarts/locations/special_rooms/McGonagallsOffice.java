package hogwarts.locations.special_rooms;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.TRANSFIGURATION_COURTYARD;
import static java.util.Arrays.asList;

public class McGonagallsOffice implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(TRANSFIGURATION_COURTYARD);
    }

    @Override
    public String getOptionText(Location currentLocation) {
        return "Go to McGonagall's office";
    }
}
