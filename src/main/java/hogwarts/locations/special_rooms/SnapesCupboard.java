package hogwarts.locations.special_rooms;

import hogwarts.locations.Location;

import java.util.List;

import static hogwarts.locations.Hogwarts.TAPESTRY_CORRIDOR;
import static java.util.Arrays.asList;

public class SnapesCupboard implements Location {

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(TAPESTRY_CORRIDOR);
    }

    @Override
    public String getOptionText(Location currentLocation) {
        return "Go to Snape's cupboard";
    }
}
