package hogwarts.locations.special_rooms;

import hogwarts.locations.GreatHall;
import hogwarts.locations.Location;
import hogwarts.locations.common_rooms.HufflepufCommonRoom;
import hogwarts.locations.corridors.GreatHallCorridor;
import hogwarts.terminal.Terminal;

import java.util.List;

import static hogwarts.locations.Hogwarts.GREAT_HALL_CORRIDOR;
import static hogwarts.locations.Hogwarts.HUFFLEPUF_COMMON_ROOM;
import static java.util.Arrays.asList;

public class Kitchen implements Location {

    @Override
    public boolean passAccessChallenge(Location currentLocation) {
        if (currentLocation instanceof HufflepufCommonRoom) return true;

        Terminal.printToScreen("You see a beautiful painting, depicting many different fruits in a bowl. You can see apples, " +
                "cherries, red grapes, green grapes and a funny looking pear. What do you want to do?");

        String input = Terminal.readCharacter().toLowerCase();

        if(input.contains("tickle") && input.contains("pear")) {
            Terminal.printToScreen("The painting swings open and you now enter the kitchen.");
            return true;
        }

        Terminal.printToScreen("Nothing happens.");
        return false;
    }

    @Override
    public List<Location> getAdjacentLocations() {
        return asList(HUFFLEPUF_COMMON_ROOM, GREAT_HALL_CORRIDOR);
    }

    @Override
    public String getOptionText(Location currentLocation) {
        if(currentLocation instanceof GreatHallCorridor) {
            return "Investigate the painting of a fruit bowl";
        }

        return "Go to the Kitchen";
    }
}
